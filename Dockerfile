FROM python:3.7

RUN mkdir /app-dir

WORKDIR /app-dir/

COPY . /app-dir/

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5555

CMD ["python", "app.py"]


