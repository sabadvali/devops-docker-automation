from flask import Flask


app = Flask(__name__)


@app.route('/')
def HelloWorld():
    return 'hello world app is working!'


app.run(host='0.0.0.0', port=5555)
